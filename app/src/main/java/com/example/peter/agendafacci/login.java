package com.example.peter.agendafacci;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }


    public void vistadocentes(View view) {

        Intent intent = new Intent(login.this,
                generarhorario.class);
        startActivity(intent);

    }

    public void vistaestudiantes(View view) {

        Intent intent = new Intent(login.this,
                MainActivity.class);
        startActivity(intent);
    }


}
