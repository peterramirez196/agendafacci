package com.example.peter.agendafacci;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    // bloqueo de pantalla

    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


}

    //MEtodo de botones

    public void vistatarea(View view) {

        Intent intent = new Intent(MainActivity.this,
                tarea.class);
        startActivity(intent);
    }
    public void vistaescogermateria(View view) {

        Intent intent = new Intent(MainActivity.this,
                escogermateria.class);
        startActivity(intent);
    }

    public void vistaverhorario(View view) {

        Intent intent = new Intent(MainActivity.this,
                generarhorario.class);
        startActivity(intent);
    }

}
