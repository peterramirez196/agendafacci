package com.example.peter.agendafacci;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CalendarView;

public class tarea extends AppCompatActivity implements CalendarView.OnDateChangeListener {


    CalendarView calendarView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tarea);
        calendarView=(CalendarView)findViewById(R.id.calendarView);
        calendarView.setOnDateChangeListener(this);
    }

    @Override
    public void onSelectedDayChange(@NonNull CalendarView calendarView, int year, int month, int dayOfMonth) {

        AlertDialog.Builder builder= new AlertDialog.Builder(this);
        final CharSequence [] items= new CharSequence[3];
        items[0]="Agregar Tarea ";
        items[1]="Ver Tarea ";
        items[2]="Cancelar ";

        final int d=dayOfMonth,m =month+1,y=year;

        builder.setTitle("Seleccione").setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(which == 0){
                    Intent intent = new Intent(tarea.this,
                            addtarea.class);
                    // para pasar los datos d,m,a
                    Bundle bundle =new Bundle();
                    bundle.putInt("dia",d);
                    bundle.putInt("mes",m);
                    bundle.putInt("anio",y);
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else if (which==1){
                    Intent intent = new Intent(tarea.this,
                            vertarea.class);
                    Bundle bundle =new Bundle();
                    bundle.putInt("dia",d);
                    bundle.putInt("mes",m);
                    bundle.putInt("anio",y);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }else{
                    return ;
                }

            }

        });

        AlertDialog dialog= builder.create();
        dialog.show();
    }
}
