package com.example.peter.agendafacci;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class escogermateria extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escogermateria);
    }

    public void vistaescogermateria(View view) {

        Intent intent = new Intent(escogermateria.this,
                generarhorario.class);
        startActivity(intent);
    }
}
